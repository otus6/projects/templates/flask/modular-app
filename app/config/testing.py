# pylint: disable=too-few-public-methods
from app.logger.base import config
from .base import Config


class TestingConfig(Config):
    """Example of configuration that can be suitable to testing.

    Args:
        Config (_type_): Parent class that has base configuration for that class.
    """
    DEVELOPMENT = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI: str = Config().change_db_uri(
        Config.SQLALCHEMY_DATABASE_URI,
        config["current_mode"]["mode"]
        )
