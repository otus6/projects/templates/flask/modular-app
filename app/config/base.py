# pylint: disable=too-few-public-methods, import-error
import os
try:
    from .local_settings import DATABASE_URL, SECRET_KEY
except ModuleNotFoundError:
    DATABASE_URL = os.environ.get("DATABASE_URL_CI")
    SECRET_KEY = os.environ.get("SECRET_KEY_CI")


class Config():
    """Base application config class which will be inheritanced by other config classes."""
    CSRF_ENABLED = True
    DEBUG = False
    DEVELOPMENT = False
    SECRET_KEY: str = SECRET_KEY
    SQLALCHEMY_DATABASE_URI: str = DATABASE_URL
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = False

    ### Below is postgres and mysql stuff from example
    # SQLALCHEMY_BINDS = {"world": MYSQL_URL}
    # IMAGE_UPLOADS = "/home/geezy/Projects/lms-on-dev/webapp/static/img/uploads"
    # ALLOWED_IMAGE_EXTENSIONS = ["PNG", "JPG", "JPEG", "GIF"]
    # MAX_IMAGE_FILESIZE = 0.5 * 1024 * 1024

    def change_db_uri(self, old_uri: str, app_mode: str) -> str:
        """Change SQLALCHEMY_DATABASE_URI to suitable current application mode and DBMS

        Args:
            old_uri (str): DATABASE_URL from local_settings.py
            app_mode (str): Current type of application mode

        Raises:
            ValueError: When database name doesn't have '-' character.
            ValueError: When database engine (DBMS) is not SQLITE, POSTRESQL and MYSQL.

        Returns:
            str: Changed SQLALCHEMY_DATABASE_URI
        """
        old_db_name = old_uri.split("/")[-1]
        base_uri = old_uri[:-len(old_db_name)]
        if old_uri.lower().startswith("sqlite"):
            prefix_idx = old_db_name.rindex(".")
            basename_db = old_db_name[:prefix_idx]
            new_db_name = basename_db + "-" + app_mode + ".db"
        elif old_uri.lower().startswith("postgresql") or old_uri.lower().startswith("mysql"):
            try:
                prefix_idx = old_db_name.rindex("-")
                basename_db = old_db_name[:prefix_idx]
            except ValueError:
                basename_db = old_db_name
            new_db_name = basename_db + "-" + app_mode
        else:
            engine = old_uri.split(":")[0]
            raise ValueError(f"Database engine '{engine}' in unknown")
        new_uri = base_uri + new_db_name
        return new_uri
