# pylint: disable=no-member, import-outside-toplevel
from app.logger import catch_exception, logger
from flask import Flask


@catch_exception(logger)
def create_app(app_mode: str = "production") -> Flask:
    """It creates pre-configured Flask application instance.

    Args:
        app_mode (str, optional): The mode in which the application was launched.
                                    Defaults to "prod".

    Returns:
        Flask: Pre-configured instance of Flask application.
    """
    from app.config import DevelopmentConfig, ProductionConfig, TestingConfig, Config

    app = Flask(__name__)
    config: Config = ProductionConfig()
    if app_mode == "development":
        config = DevelopmentConfig()
    elif app_mode == "testing":
        config = TestingConfig()
    app.config.from_object(config)
    app.jinja_env.add_extension("jinja2.ext.loopcontrols")

    from app.models.base import db, migrate

    db.init_app(app)
    migrate.init_app(app, db)

    from app.views.home import home
    from app.views.query import query_page

    app.register_blueprint(home)
    app.register_blueprint(query_page)

    return app
