# pylint: disable=inconsistent-return-statements
from app.forms.choice_form import ChoiceForm
from app.logger import logger, catch_exception
from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound


query_page = Blueprint(
    "query_page", __name__, template_folder="templates", static_folder="static"
)


@query_page.route("/query", methods=["GET", "POST"])
@catch_exception(logger)
def show():
    """It returns rendered html.

    Returns:
        str: HTML webpage.
    """
    form = ChoiceForm()
    if form.validate_on_submit():
        context = {
            "title": "result",
            "result": form.opts.data
        }
        try:
            return render_template(
                "query_page.html", **context
            )
        except TemplateNotFound:
            abort(404)
    context = {
        "form": form
    }
    try:
        return render_template("query_page.html", **context)
    except TemplateNotFound:
        abort(404)
