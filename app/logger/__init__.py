from .base import create_logger, catch_exception, get_app_mode

logger = create_logger()
