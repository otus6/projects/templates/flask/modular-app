# pylint: disable=broad-except
from configparser import ConfigParser
import logging.config
import logging
from os.path import abspath, dirname, join
from typing import Any, Optional


BASEDIR: str = abspath(dirname(__file__))
CONFIGNAME: str = abspath(join(BASEDIR, "config.ini"))
LOGS_DIR: str = join(BASEDIR, "../../logs")

config = ConfigParser()
config.read(CONFIGNAME)


def get_app_mode(app_mode: Optional[str] = "production") -> None:
    """This function gets application running mode from run.py
        and writes it to config file of logger in [current_mode] section.

    Args:
        dev_mode (str, optional): Not None if "development" was passed. Defaults to None.
    """
    app_mode_dict: dict[str, str] = {"mode": "production", "filename": "app"}
    if app_mode == "development":
        app_mode_dict["mode"] = "development"
        app_mode_dict["filename"] = "app-development"
        config["current_mode"] = app_mode_dict
        with open(CONFIGNAME, "w", encoding="utf-8") as configfile:
            config.write(configfile)
    elif app_mode == "testing":
        app_mode_dict["mode"] = "testing"
        app_mode_dict["filename"] = "app-testing"
        config["current_mode"] = app_mode_dict
        with open(CONFIGNAME, "w", encoding="utf-8") as configfile:
            config.write(configfile)
    else:
        config["current_mode"] = app_mode_dict
        with open(CONFIGNAME, "w", encoding="utf-8") as configfile:
            config.write(configfile)


def create_logger() -> logging.Logger:
    """It creates pre-configured instance of logger.

    Returns:
        Logger: Logger instance
    """
    app_mode: str = config["current_mode"]["mode"]
    filename: str = config["current_mode"]["filename"]
    logging.config.fileConfig(
        CONFIGNAME,
        disable_existing_loggers=True,
        defaults={"logfilename": f"{LOGS_DIR}/{filename}.log"},
    )
    logger = logging.getLogger(app_mode)
    return logger


def try_execute(func: Any, logger: logging.Logger, args: Any, kwargs: Any) -> tuple[Any, bool]:
    """It executes passed command and returns tuple as result.

    Args:
        func (Any): Passed command or function.
        logger (Logger): Logger instance
        args (Any): All positional arguments.
        kwargs (Any): All keyword arguments.

    Returns:
        tuple: (string - command result or error message, boolean - indicates if is error)
    """
    try:
        return (func(*args, **kwargs), False)
    except Exception as error:
        logger.exception(f"There was an exception in {func.__name__}() function")
        return (error, True)


def catch_exception(logger: logging.Logger) -> Any:
    """
    A decorator that wraps the passed in function and logs
    exceptions should one occur

    @param logger: The logging object
    """

    def decorator(func):
        def wrapper(*args, **kwargs):
            response, is_error = try_execute(func, logger, args, kwargs)
            if is_error:
                raise response
            return response

        return wrapper

    return decorator
