# What is it?

This is the template project for Flask applications with modular based structure. It consist all minimal requirements and code base for the fast start with developing your own application. Just fork it, create views, bring static, configure database and of course install all requrements to get your own brand new application.

# Referrals links (Offical documentation)

- https://flask.palletsprojects.com/en/2.1.x/
- https://flask-migrate.readthedocs.io/en/latest/index.html
- https://docs.pytest.org/en/6.2.x/usage.html
- https://docs.sqlalchemy.org/en/14/intro.html

# Installation

- `git clone https://`
- `source .venv/bin/activate` 
- `pip install -r requirements.txt` 


# Usage and examples
```
usage: run.py [-h] [-d] [-p PORT] [-H HOST]

This is the flask template application.

options:
  -h, --help            show this help message and exit
  -d, --dev             It enables development mode of application. WARNING! By default (without this key) it works in production mode.
  -p PORT, --port PORT  On this port will run the application. Defaults are 8001 for development and 5000 for production modes.
  -H HOST, --host HOST  IP address on that application will run. Default 0.0.0.0 for all modes.
```
To run application in default (production) mode just execute in console run.py
`./run.py`
Development mode runs via -d, --dev options
`./run.py -d` 

To run tests via pytest just execute:
`python3 -m pytest -vvv --cov=app`

To run pylint use:
`pylint $(git ls-files '*.py' | grep -v backups) || pylint-exit --error-fail $?`

# Structure

```
.
├── app
│   ├── config
│   ├── forms
│   ├── logger
│   ├── models
│   ├── static
│   ├── templates
│   └── views
├── logs
├── migrations
│   └── versions
└── tests
    ├── functional
    └── unit
```

# Configuration
Aplication and DB in app/config/
Logger in app/logger/config.ini
Migrations in migrations/{env.py,alembic.ini}

# Migrations
Offical documentation of Flask-Migrate:
https://flask-migrate.readthedocs.io/en/latest/index.html

### Long story shot about how to start from scratch
1) Start up instance of DBMS and create DB into it (for SQLite *.db file will create with flask db migrate)
2) Configure application to work with this DBMS and check that it's work (for SQLite mostly done)
3) Create a table(-s) and its (theirs) structure (fill in fields/types etc.)
`python3 -m flask db init && python3 -m flask db migrate -m "Initial migration" && python3 -m flask db upgrade`
also it creates "migrations" folder in the project root
4) Fill the table(-s) with content via Python or something else on our own!

# WORKAROUND IF FLASK-MIGRATE DETECTS NO CHANGES:
  - add `from app.models import mymodel-1, mymodel-2` your model imports to env.py in ther root of 'migrations' folder
https://stackoverflow.com/questions/51783300/flask-migrate-no-changes-detected-to-schema-on-first-migration