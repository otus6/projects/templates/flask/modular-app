# pylint: disable=import-error, no-member, too-few-public-methods
from app.models.base import db
from app.models.choice import Choice, choice_query
import pytest
from sqlalchemy import null


@pytest.mark.usefixtures("prepare_db")
class TestModels:
    """Class for unit testing of application models.
    """
    def test_choice_query(self):
        """Tests for Choice model
        """
        assert db.session.query(Choice).count() == 6
        assert choice_query().filter(Choice.extra == null()).count() == 3
