from flask_wtf import FlaskForm 
from app.models.choice import choice_query
from wtforms_sqlalchemy.fields import QuerySelectField


class ChoiceForm(FlaskForm):
    opts = QuerySelectField(query_factory=choice_query, allow_blank=True, get_label='name')
