# pylint: disable=no-member, too-few-public-methods
from .base import db
from flask_sqlalchemy import BaseQuery
from flask_sqlalchemy.model import DefaultMeta
from sqlalchemy import Integer, String, Column


BaseModel: DefaultMeta = db.Model


class Choice(BaseModel): # type: ignore
    """Model for query view.
    it seems mypy raised error due to this bug: https://github.com/python/mypy/issues/5865
    This solution should work and it was suggested here: https://github.com/dropbox/sqlalchemy-stubs/issues/76#issuecomment-595839159

    Args:
        db(SQLAlchemy): SQLAlchemy database object.

    """
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    extra = Column(String(50))

    def __repr__(self) -> str:
        return f"[Choice {self.name}]"


def choice_query() -> BaseQuery:
    """Query factory for ChoiceForm.

    Returns:
        BaseQuery: Filtered database response.
    """
    return Choice.query




# class Choice(db.Model):
#     """Model for query view.

#     Args:
#         db(SQLAlchemy): SQLAlchemy database object.

#     """
#     id: int = db.Column(db.Integer, primary_key=True)
#     name: str = db.Column(db.String(50))
#     extra: str = db.Column(db.String(50))

#     def __repr__(self) -> str:
#         return f"[Choice {self.name}]"


# def choice_query() -> db.SQLAlchemy:
#     """Query factory for ChoiceForm.

#     Returns:
#         db(SQLAlchemy): Filtered database response.
#     """
#     return Choice.query
