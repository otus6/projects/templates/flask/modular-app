# exception_decor.py


def get_error(func, logger, args, kwargs):
    try:
        return (func(*args, **kwargs), False)
    except Exception as error:
        # log the exception
        logger.exception(f"There was an exception in {func.__name__}() function")
        return (error, True)


def exception(logger):
    """
    A decorator that wraps the passed in function and logs 
    exceptions should one occur
    
    @param logger: The logging object
    """
    
    def decorator(func):
    
        def wrapper(*args, **kwargs):
            result, error = get_error(func, logger, args, kwargs)
            if error:
                raise result
            return result 
        return wrapper
    return decorator