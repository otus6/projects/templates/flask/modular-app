# pylint: disable=import-error
from app import create_app
from app.create_db import init_db, db
from app.logger import get_app_mode
from app.models.choice import Choice, choice_query
from faker import Faker
from sqlalchemy import null


app = create_app("testing")
get_app_mode()



class TestModels:
    
    def setup(self):
        init_db()
        
        with app.app_context():
            db.session.query(Choice).delete()
            db.session.commit()

        fake = Faker()
        for _ in range(3):
            name = fake.name()
            choice = Choice(name=name)
            with app.app_context():
                db.session.add(choice)
                db.session.commit()


        for _ in range(3):
            name = fake.name()
            text = fake.text()
            choice = Choice(name=name, extra=text)
            with app.app_context():
                db.session.add(choice)
                db.session.commit()
    

    def test_choice_query(self):
        with app.app_context():
            assert db.session.query(Choice).count() == 6
            assert choice_query().filter(Choice.extra == null()).count() == 3


    def teardown(self):
        pass