#!/usr/bin/env python3
from app import create_app
from app.logger import get_app_mode
import argparse
import os


def init_parser() -> argparse.Namespace:
    """It initializes argument parser for Flask application.

    Returns:
        Namespace: Passed arguments to Flask application.
    """
    parser = argparse.ArgumentParser(
        description="This is the flask template application."
    )
    parser.add_argument(
        "-d",
        "--dev",
        help=(
            "It enables development mode of application. "
            "WARNING! By default (without this key) it works in production mode."
            ),
        action="store_true",
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        help=(
            "On this port will run the application. "
            "Defaults are 8001 for development and 5000 for production modes."
        ),
    )
    parser.add_argument(
        "-H",
        "--host",
        type=str,
        help="IP address on that application will run. Default 0.0.0.0 for all modes.",
    )
    args = parser.parse_args()

    return args


def main() -> None:
    """Main function, that runs Flask application.
    """
    host = "0.0.0.0"
    port = 5000
    debug = False

    args = init_parser()

    if args.port:
        port = args.port
    if args.host:
        host = args.host
    if args.dev:
        app_mode = "development"
        os.environ["FLASK_ENV"] = app_mode
        get_app_mode(app_mode)
        if not args.port:
            port = 8001
        debug = True
        app = create_app(app_mode)
        app.run(host=host, port=port, debug=debug)
    else:
        get_app_mode()
        app = create_app()
        app.run(host=host, port=port, debug=debug)


if __name__ == "__main__":
    main()
